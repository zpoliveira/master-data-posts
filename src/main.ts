import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger, ValidationPipe } from '@nestjs/common';
const config = require('config');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  app.enableCors({ origin: '*', optionsSuccessStatus: 200 });

  swaggerSetUp(app);

  const logger = new Logger();

  await app.listen(config.get('serverPort'));

  logger.log(`Post Service is listening at http://localhost:${config.get('serverPort')}!`)


  function swaggerSetUp(app) {
    const options = new DocumentBuilder()
      .setTitle('POST SERVICE')
      .setDescription('POST SERVICE')
      .setVersion('1.0')
      .build();

    const doc = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('endpoints', app, doc);
  }
}

bootstrap();
