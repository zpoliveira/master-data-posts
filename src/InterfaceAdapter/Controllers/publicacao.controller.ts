import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { ApiBody, ApiOkResponse, ApiParam, ApiTags } from "@nestjs/swagger";
import { CreatePublicacaoDto } from "src/Services/dtos/controller/createPublicacao.dto";
import { ResponseCreatePublicacaoDto } from "src/Services/dtos/controller/responseDto/responseCreatePublicacao.dto";
import { ResponseGetUserPostDto } from "src/Services/dtos/controller/responseDto/responseGetUserPost.dto";
import { PublicacaoService } from "src/Services/publicacao.service";

@Controller('publicacao')
@ApiTags('publicacao')
export class PublicacaoController {
  constructor(
    private readonly publicacaoService: PublicacaoService
  ) { }

  @Get()
  async listarTodos(): Promise<any[]> {
    return this.publicacaoService.findAll();
  }

  @Post()
  @ApiBody({ type: CreatePublicacaoDto })
  @ApiOkResponse({ type: ResponseCreatePublicacaoDto })
  async criar(@Body() publicacaoDto: CreatePublicacaoDto): Promise<ResponseCreatePublicacaoDto> {
    return this.publicacaoService.create(publicacaoDto);
  }



  @Get('listarPublicacaoJogador/:email')
  @ApiParam({ 
    name: 'email',
    type: 'string', 
    allowEmptyValue: false,
    required: true })
    @ApiOkResponse({ type: ResponseGetUserPostDto })
  async getUserPost(@Param() params): Promise<ResponseGetUserPostDto[]> {
    try{
     return this.publicacaoService.getUserPost(params.email);
    }catch(e){
      console.log('Error caught',e);
    }
  }


}