import { POSTS_PROVIDER, PUBLICACAO_MODEL_PROVIDER } from "constants/constants";
import { Connection } from "mongoose";
import { PublicacaoSchema } from "./publicacao.schema";

export const publicacaoProviders = [
    {
        provide: PUBLICACAO_MODEL_PROVIDER,
        useFactory: (connection: Connection) => connection.model('Publicacao', PublicacaoSchema),
        inject: [POSTS_PROVIDER],
    },
];