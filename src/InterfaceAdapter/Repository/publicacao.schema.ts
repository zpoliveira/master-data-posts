import { Schema } from 'mongoose';
import { comentarioSchema } from './comentario.schema';
import { Publicacao } from 'src/Domain/publicacao';

export const PublicacaoSchema = new Schema<Publicacao>({
    emailJogador: String,
    titulo: String,
    texto: String,
    likes: Number,
    dislikes: Number,
    comentarios: { type: [comentarioSchema], required: false },
    tags: [String],
    dataHora: Date
})

