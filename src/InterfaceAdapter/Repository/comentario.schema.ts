import { tagSchema } from './tag.schema';
import { Schema } from "mongoose";
import { CreateComentarioDto } from "../../Services/dtos/controller/createComentario.dto";
import { Comentario } from 'src/Domain/comentario';


export const comentarioSchema = new Schema<Comentario>({
    emailJogador: String,
    likes: Number,
    dislikes: Number,
    tags: [tagSchema],
    dataHora: Date,
    texto: String
})
