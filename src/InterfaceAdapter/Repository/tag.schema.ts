import { Schema } from 'mongoose';
import { Tag } from 'src/Domain/tag';

export const tagSchema = new Schema<Tag>({
    tag: String
})
