import { POSTS_PROVIDER } from 'constants/constants';
import * as mongoose from 'mongoose';
const config = require('config');

export const databaseProviders = [
  {
    provide: POSTS_PROVIDER,
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect(config.get('mongooseLink')),
  },
];
