import { Inject, Injectable } from "@nestjs/common";
import { PUBLICACAO_MODEL_PROVIDER } from "constants/constants";
import { Model } from "mongoose";
import { Publicacao } from "src/Domain/publicacao";
import { ResponseCreatePublicacaoDto } from "src/Services/dtos/controller/responseDto/responseCreatePublicacao.dto";
import { RepoPublicacaoDto } from "src/Services/dtos/repository/repoPublicacao.dto";

@Injectable()
export class PublicacaoRepository{
    constructor(
        @Inject(PUBLICACAO_MODEL_PROVIDER) private readonly publicacaoModel: Model<Publicacao>) { }

    async create(repoPublicacaoDto: RepoPublicacaoDto): Promise<any> {
        const createdPost = new this.publicacaoModel(repoPublicacaoDto);
        let post = await createdPost.save();
        let { _id } = post;
        return { _id };
    }

    async findAll(): Promise<RepoPublicacaoDto[]> {
        return await this.publicacaoModel.find().lean();
    }

    async getUserPost(userEmail:string): Promise<RepoPublicacaoDto[]> {
                
        return this.publicacaoModel.find({ emailJogador: userEmail }).lean();         
    }
}