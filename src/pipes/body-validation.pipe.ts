import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
@Injectable()
export class BodyValidationPipe implements PipeTransform<any> {

    async transform(value: any, { metatype }: ArgumentMetadata) {
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }
        const object = plainToClass(metatype, value);
        const errors = await validate(object);
        if (errors.length > 0) {
            const constraints = errors.map(err => {
                if (err.constraints && err.children.length === 0) {
                    return err.constraints;
                }
                const [result] = this.getConstraintsFromNestedObjects(err.children);
                return result;
            });
            throw new BadRequestException();

        }
        return object;
    }

    private toValidate(metatype: Function): boolean {
        const types: Function[] = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }

    private getConstraintsFromNestedObjects(children) {
        if (children.length === 0) {
            return;
        }
        return children.map(err => {
            if (err.constraints) {
                return err.constraints;
            }
            return this.getConstraintsFromNestedObjects(err.children);
        });
    }
}
