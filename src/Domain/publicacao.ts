import { IEntity } from 'src/Shared/IEntity';
import { IsArray, IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { Comentario } from './comentario';

export class Publicacao implements IEntity {

    @IsString()
    @IsNotEmpty()
    emailJogador: string;

    @IsString()
    @IsNotEmpty()
    titulo: string;

    @IsString()
    @IsNotEmpty()
    texto: string;

    @IsNumber()
    @IsNotEmpty()
    likes: number;

    @IsNumber()
    @IsNotEmpty()
    dislikes: number;

    @IsArray()
    @IsOptional()
    comentarios?: Array<Comentario>;

    @IsArray()
    @IsNotEmpty()
    tags: Array<string>;

    @IsDate()
    @IsNotEmpty()
    dataHora: Date;
}