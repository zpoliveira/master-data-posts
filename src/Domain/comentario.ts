import { IsArray, IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { IEntity } from 'src/Shared/IEntity';
import { Tag } from './tag';

export class Comentario implements IEntity {

    @IsString()
    @IsNotEmpty()
    emailJogador: string;

    @IsNumber()
    @IsNotEmpty()
    likes: number;

    @IsNumber()
    @IsNotEmpty()
    dislikes: number;

    @IsArray()
    @IsOptional()
    tags: Array<Tag>;

    @IsDate()
    @IsNotEmpty()
    dataHora: Date;

    @IsString()
    @IsNotEmpty()
    texto: string;
}