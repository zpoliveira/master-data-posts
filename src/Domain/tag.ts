import { IsNotEmpty, IsString } from 'class-validator';
import { IEntity } from 'src/Shared/IEntity';

export class Tag implements IEntity {

    constructor(tag: string) {
        this.tag = tag;
    }

    @IsString()
    @IsNotEmpty()
    tag: string;
}