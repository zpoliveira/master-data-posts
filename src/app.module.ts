import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PublicacaoModule } from './Services/modules/publicacao.module';
import { ComentarioModule } from './Services/modules/comentario.module';
import { TagModule } from './Services/modules/tag.module';
const config = require('config');

@Module({
  imports: [ConfigModule.forRoot({ 
    isGlobal: true 
  }), 
    PublicacaoModule, ComentarioModule, TagModule, 
    ],
  controllers: [],
  providers: [],
})
export class AppModule {}
