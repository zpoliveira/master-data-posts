import { Module } from '@nestjs/common';
import { PublicacaoRepository } from 'src/Infrastructure/repository/publicacaoRepository';
import { PublicacaoController } from 'src/InterfaceAdapter/Controllers/publicacao.controller';
import { publicacaoProviders } from '../../InterfaceAdapter/Repository/publicacao.providers';
import { PublicacaoService } from '../publicacao.service';
import { DatabaseModule } from './database.module';

@Module({
    imports: [DatabaseModule],
    controllers: [PublicacaoController],
    providers: [
        PublicacaoService,
        ...publicacaoProviders, PublicacaoRepository]
})
export class PublicacaoModule {}
