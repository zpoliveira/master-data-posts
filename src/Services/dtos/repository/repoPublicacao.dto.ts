import { Publicacao } from './../../../Domain/publicacao';
import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsDate, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { RepoComentarioDto } from "./repoComentario.dto";
import { ResponseGetUserPostDto } from '../controller/responseDto/responseGetUserPost.dto';


export class RepoPublicacaoDto {

    constructor(
        publicacao: Publicacao | any
    ) {
        this.emailJogador = publicacao.emailJogador;
        this.titulo = publicacao.titulo;
        this.texto = publicacao.texto;
        this.likes = publicacao.likes;
        this.dislikes = publicacao.dislikes;
        this.comentarios = publicacao.comentarios;
        this.tags = publicacao.tags;
        this.dataHora = publicacao.dataHora;
    }


    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly emailJogador: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly titulo: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly texto: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly likes: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly dislikes: number;

    @IsArray()
    @IsOptional()
    @ApiProperty({ type: RepoComentarioDto })
    readonly comentarios?: RepoComentarioDto[]

    @IsArray()
    @IsOptional()
    @ApiProperty({ type: String })
    readonly tags: string[];

    @IsDate()
    @IsNotEmpty()
    @ApiProperty()
    readonly dataHora: Date;

    @IsMongoId()
    @ApiProperty()
    readonly _id?: string;
}

export function toResponsePostListDto(repoPost: RepoPublicacaoDto[]): ResponseGetUserPostDto[] {
    return repoPost.map(post=>new ResponseGetUserPostDto(post));
}
