import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsDate, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { Schema } from 'mongoose';
import { ResponseCreateComentarioDto } from "../controller/responseDto/responseCreateComentario.dto";
import { RepoTagDto } from "./repoTag.dto";

export class RepoComentarioDto {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly emailJogador: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly likes: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly dislikes: number;

    @IsArray()
    @IsOptional()
    @ApiProperty({type: RepoTagDto})
    readonly tags: RepoTagDto[];

    @IsDate()
    @IsNotEmpty()
    @ApiProperty()
    readonly dataHora: Date;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly texto: string;

    @IsMongoId()
    @ApiProperty()
    readonly _id: Schema.Types.ObjectId;

}

export const toResponseDto = (post: RepoComentarioDto): ResponseCreateComentarioDto => {
    return {
        _id: post._id
    }
}
