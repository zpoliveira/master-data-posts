import { IsMongoId, IsString } from 'class-validator';
import { ApiProperty } from "@nestjs/swagger";
import { Schema } from 'mongoose';

export class RepoTagDto {
    @IsString()
    @ApiProperty()
    readonly tag: string;
}
