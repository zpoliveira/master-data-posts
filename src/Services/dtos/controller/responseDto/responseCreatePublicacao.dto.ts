import { ApiProperty } from "@nestjs/swagger";
import { IsMongoId } from "class-validator";
import { Schema } from 'mongoose';

export class ResponseCreatePublicacaoDto {

    @IsMongoId()
    @ApiProperty()
    readonly _id: Schema.Types.ObjectId;

}