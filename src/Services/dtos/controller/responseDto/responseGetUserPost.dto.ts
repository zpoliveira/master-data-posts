import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { RepoPublicacaoDto } from "../../repository/repoPublicacao.dto";
import { ResponsePostCommentListDto } from "./responsePostCommentList.dto";


export class ResponseGetUserPostDto {

    constructor(repoPost: RepoPublicacaoDto){
        
        this.titulo = repoPost.titulo;
        this.dataHora = repoPost.dataHora;
        this.texto = repoPost.texto;
        this.likes = repoPost.likes;
        this.dislikes= repoPost.dislikes;
        this.comentarios = repoPost.comentarios.map(cm=> new ResponsePostCommentListDto(cm));
       
    }

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly titulo: string;
    
    @IsDate()
    @IsNotEmpty()
    @ApiProperty()
    readonly dataHora: Date;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly texto: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly likes: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly dislikes: number;

    @IsArray()
    @IsOptional()
    @ApiProperty({type: ResponsePostCommentListDto})
    readonly comentarios?: ResponsePostCommentListDto[];
}
