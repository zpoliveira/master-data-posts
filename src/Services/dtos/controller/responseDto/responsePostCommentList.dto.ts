import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsNotEmpty, IsString } from "class-validator";
import { RepoComentarioDto } from "../../repository/repoComentario.dto";


export class ResponsePostCommentListDto {

    constructor(
        comentario: RepoComentarioDto
    ){
        
        this.emailJogador = comentario.emailJogador;
        this.dataHora = comentario.dataHora;
        this.texto = comentario.texto;
    }

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly emailJogador: string;

    @IsDate()
    @IsNotEmpty()
    @ApiProperty()
    readonly dataHora: Date;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly texto: string;

}

