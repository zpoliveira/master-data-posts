import { IsString } from 'class-validator';
import { ApiProperty } from "@nestjs/swagger";

export class CreateTagDto {
    @IsString()
    @ApiProperty()
    readonly tag: string;

}
