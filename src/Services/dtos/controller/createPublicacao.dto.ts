import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsDateString, IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class CreatePublicacaoDto {

    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({description: 'email do jogador'})
    readonly emailJogador: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({description: 'Titulo da publicação'})
    readonly titulo: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({description: 'texto da publicação'})
    readonly texto: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({description: 'número de likes'})
    likes: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({description: 'número de dislikes'})
    dislikes: number;

    @IsArray()
    @IsOptional()
    @ApiProperty({description: 'lista de tags', type: [String]})
    readonly tags: string[];

    @IsDateString()
    @IsNotEmpty()
    @ApiProperty({description: 'dataHora'})
    dataHora: Date;

}
