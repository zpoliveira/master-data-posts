import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { Publicacao } from "src/Domain/publicacao";
import { CreateTagDto } from "./createTag.dto";

export class CreateComentarioDto {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly emailJogador: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly likes: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    readonly dislikes: number;

    @IsArray()
    @IsOptional()
    @ApiProperty({type: CreateTagDto})
    readonly tags: CreateTagDto[];

    @IsDate()
    @IsNotEmpty()
    @ApiProperty()
    readonly dataHora: Date;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly texto: string;

}

