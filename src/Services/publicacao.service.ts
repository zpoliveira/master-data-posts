
import { CreatePublicacaoDto } from './dtos/controller/createPublicacao.dto';
import { PublicacaoRepository } from 'src/Infrastructure/repository/publicacaoRepository';
import { RepoPublicacaoDto, toResponsePostListDto } from './dtos/repository/repoPublicacao.dto';
import { Injectable } from '@nestjs/common';
import { ResponseCreatePublicacaoDto } from './dtos/controller/responseDto/responseCreatePublicacao.dto';
import { ResponseGetUserPostDto } from './dtos/controller/responseDto/responseGetUserPost.dto';


@Injectable()
export class PublicacaoService {
    constructor(private publicacaoRepository: PublicacaoRepository) { 

    }

    async create(createPublicacaoDto: CreatePublicacaoDto): Promise<ResponseCreatePublicacaoDto> {
        let repo: RepoPublicacaoDto = {
            ...createPublicacaoDto
        }
        return await this.publicacaoRepository.create(repo);
    }

    async findAll(): Promise<RepoPublicacaoDto[]> {
        return await this.publicacaoRepository.findAll();
    }


    async getUserPost(userEmail:string): Promise<ResponseGetUserPostDto[]> {
    
        let postList = await this.publicacaoRepository.getUserPost(userEmail);
        return toResponsePostListDto(postList);
   
    }
}